SCH2PS=./sch2ps
SCHEM=schematic.sch
POSTSCRIPT=$(patsubst %.sch,%.ps,${SCHEM})

all: schematic.pdf

schematic.pdf: $(POSTSCRIPT)
	cat  $(POSTSCRIPT) | ps2pdf - $@

clean:
	rm *.ps *.pdf *~ -rf

%.ps: %.sch
	$(SCH2PS) $<

